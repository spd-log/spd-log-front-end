import { actionTypes as userActionTypes } from 'reducers/user'
import { api } from '../config';

import history from 'utils/history';
import store from 'store';


export async function loginUser(email, password) {
  const creds = {
    'username': email,
    'password': password
  }
  let res = await api.post('/auth/login', creds);
  console.log(res);

  // console.log(email, password);
  // let res = {
  //   status: 200,
  //   data: {
  //     id: 123,
  //     firstName: 'First name',
  //     lastName: 'last name',
  //     role: 'user'
  //   }
  // }
  if (res.status !== 200) {
    throw new Error('Something went wrong')
  }
  let userInfo = JSON.stringify(res.data)
  localStorage.setItem('user', userInfo)
  store.dispatch({ type: userActionTypes.USER_SET, payload: res.data })
}


export function logout() {
  localStorage.removeItem('user');
  store.dispatch({ type: userActionTypes.USER_UNSET })
  history.replace('/login')
}

export function authUser() {
  const user = localStorage.getItem('user');
  return user;
}