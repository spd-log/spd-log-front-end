import React from 'react';
import './profile.css';

const Profile = (props) => {
    let { firstName, lastName, imgUrl, achievements, level } = props;

    //заглушка
    level = 5;
    firstName = 'Василь';
    lastName = 'Петренко';
    imgUrl = 'https://i.gifer.com/IUbK.gif';
    achievements = [{img: 'https://images.ua.prom.st/2007756467_w128_h128_verhivka-zirka-yalinkova-plastik.jpg', text: 'Ачівка 1'},
        {img: 'https://images.ua.prom.st/2007756467_w128_h128_verhivka-zirka-yalinkova-plastik.jpg', text: 'Ачівка 2'},
        {img: 'https://images.ua.prom.st/2007756467_w128_h128_verhivka-zirka-yalinkova-plastik.jpg', text: 'Ачівка 3'}];
    //конец заглушки

    return (
        <div className="profile-container">
            <div className="name">
                <span>{firstName} </span>
                <span>{lastName}</span>
            </div>
            <div className="avatar">
                <img className="avatar-img" src={imgUrl} alt="Avatar"/>
                <div className="level">
                    <span>{level}</span>
                </div>
            </div>
            <div className="achievements-container">
                <div className="achievement">
                    <img className="achievement-img" src={achievements[0].img} alt="achievement"/>
                    <span>{achievements[0].text}</span>
                </div>
                <div className="achievement">
                    <img className="achievement-img" src={achievements[1].img} alt="achievement"/>
                    <span>{achievements[1].text}</span>
                </div>
                <div className="achievement">
                    <img className="achievement-img" src={achievements[2].img} alt="achievement"/>
                    <span>{achievements[2].text}</span>
                </div>
            </div>
        </div>
    );
};

export default Profile;