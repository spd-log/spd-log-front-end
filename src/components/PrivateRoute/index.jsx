import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { getUser } from '../../config'

export const PrivateRoute = props => {
  // if (userRoles.some(userRole => props.roles.includes(userRole))) allow = true
  // const notAllow = _.isEmpty(props.user) //delete && false for prod
  const userData = getUser();
  if (!userData || !userData.id) {
    console.log('back to login')
    return <Redirect to={'/login'} />
  }

  console.log(props);

  // if (!props.roles.some(v => v === props.user.role)) {
  //   allow = false
  // }

  return !props.user.id ? <Redirect to={'/not-found'} /> : <Route {...props} />
}

export default connect(store => ({ user: store.user }))(PrivateRoute)
