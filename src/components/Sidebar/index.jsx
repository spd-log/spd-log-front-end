import React from 'react';
import { Button } from 'react-bootstrap';
import Profile from 'components/Profile';
import { NavLink } from 'react-router-dom';

import * as userActions from '../../actions/user';

import './styles.scss';
const Sidebar = () => {
  const logout = () => {
    userActions.logout();
  };

  return (
      <div className="sidebar">
        <Profile />
        <ul className="flex-column">
          <li>
            <NavLink to="/lectures">Лекції</NavLink>
          </li>
          <li>
            <NavLink to="/calendar">Календар</NavLink>
          </li>
          <li>
            <NavLink to="/journal">Журнал</NavLink>
          </li>
        </ul>
        <Button variant="info" onClick={() => logout()}>
          Вийти
        </Button>
      </div>
  );
};

export default Sidebar;
