export const actionTypes = {
  USER_SET: 'USER_SET',
  USER_UNSET: 'USER_UNSET',
}

export const initState = {
  id: '',
  firstName: '',
  lastName: ''
}

export function reducer(state = initState, action) {
  switch (action.type) {
    case actionTypes.USER_SET:
      return { ...state, ...action.payload }
    case actionTypes.USER_UNSET:
      return initState
    default:
      return state
  }
}
