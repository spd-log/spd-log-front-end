import { combineReducers } from 'redux'
import * as userReducer from './user'

export const initialState = {
  user: userReducer.initState,
}

export const rootReducer = combineReducers({
  user: userReducer.reducer
})
