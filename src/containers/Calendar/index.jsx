import React, { useEffect } from 'react';

const Calendar = () => {
  // useEffect(() => {
  //   effect
  //   return () => {
  //     cleanup
  //   };
  // }, [input])

  return (
    <>
        <iframe
          src="https://calendar.google.com/calendar/embed?src=fse941elgoja3qvvkq0u6v4l7c%40group.calendar.google.com&ctz=Europe%2FHelsinki"
          style={{ border: 0, margin: '0 auto'}}
          width="100%"
          height="100%"
          frameBorder="0"
          scrolling="no"
          title="calendar"
        >
          Calendar
        </iframe>
    </>
  );
};

export default Calendar;
