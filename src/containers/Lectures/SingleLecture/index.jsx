import React, { useEffect } from 'react';
import Sidebar from 'components/Sidebar';
import { Accordion, Card, Button } from 'react-bootstrap';
import './singleLecture.css';

const SingleLecture = (props) => {
  let { videos } = props;

  //the Zaglushka
  videos = [
    { title: 'Відос від Льоні', url: 'https://youtu.be/uYGUz4gUzFk' },
    { title: 'Відос від Льоні 2', url: 'https://youtu.be/uYGUz4gUzFk' }
  ];
  //the end of Zaglushka

  videos = videos.map((obj, i) => (
    <li key={i}>
      <a href={obj.url}>{obj.title}</a>
    </li>
  ));

  return (
    <>
      <div className="content-wrapper">
        <div className="right-side">
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Відео
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  <ul>{videos}</ul>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Презентація
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>
                  <iframe
                    src="https://drive.google.com/file/d/1Dk2BfSME1vVTNKvVzSbGCHW_Zb8eg4ql/preview"
                    width="640"
                    height="480"
                  >
                    Презентація
                  </iframe>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="2">
                  Додаткові матеріали
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="2">
                <Card.Body>
                  <ul>
                    <li>
                      <a href="https://habr.com/ru/post/50147/">
                        Типы HTTP-запросов и философия REST
                      </a>
                    </li>
                    <li>
                      <a href="https://developer.mozilla.org/ru/docs/Web/HTTP/%D0%9A%D1%83%D0%BA%D0%B8">
                        Куки HTTP
                      </a>
                    </li>
                  </ul>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Домашнє завдання
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>
                  <div className="asd">
                    Попередню домашню роботу (Error Handling) розбити за
                    модулями, винесеними в окремі файли (rectangle.js, point.js
                    і т д). Прибрати перевірку на валідність типів, замінивши її
                    статичною типізацією. В директорії src.ts розмістити код на
                    TS, в директорії src.flow розмістити код на Flow. Створити
                    дві окремі конфігурації webpack для роботи з кодом на ts та
                    з кодом на flow. Додати фічі, яких і ще немає в стандарті
                    ES, проте які знаходяться в stage 1-4, для транспайлингу
                    коду цих фічі потрібно додати відповідні babel плагіни.
                    Посилання на практичну роботу:
                    <a href="https://gitlab.com/spdu19/practice_preprocessing">
                      gitlab.com
                    </a>
                  </div>
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </div>
      </div>
    </>
  );
};

export default SingleLecture;
