import React from 'react';
import Sidebar from 'components/Sidebar';

const dummyData = [
  {
    id: 1,
    title: 'Lecture 1'
  },
  {
    id: 2,
    title: 'Lecture 2'
  },
  {
    id: 3,
    title: 'Lecture 3'
  },
  {
    id: 4,
    title: 'Lecture 4'
  }
]

const Lectures = ({history}) => {

  const goToSingleArticle = (id) => {
    history.push(`lectures/${id}`)
  }

  const articleList = dummyData.map( article => (
    <li key={article.id}>
      <h4 onClick={() => goToSingleArticle(article.id)}>{article.title}</h4>
    </li>
  ))

  return (
    <>
    <div className="content-wrapper">
      <div className="right-content">
        <h2>Список лекцій</h2> 
        <ul>
          {articleList}
        </ul>
      </div>
    </div>
  </>
  )
};

export default Lectures;
