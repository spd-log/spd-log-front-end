import React from 'react';

const Journal = () => {
  // useEffect(() => {
  //   effect
  //   return () => {
  //     cleanup
  //   };
  // }, [input])

  return (
    <>
        <iframe
          src="https://docs.google.com/spreadsheets/d/1i05eDZl06PasUujQ1hCrZJKT7vtwSpE2uD-U_DsW4yc/edit?ts=5da497bc#gid=0"
          style={{ border: 0, margin: '0 auto'}}
          width="100%"
          height="100%"
          frameBorder="0"
          scrolling="no"
          title="journal"
        >
          Табель
        </iframe>
    </>
  );
};

export default Journal;
