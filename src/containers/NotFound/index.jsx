import React from 'react';
import { Link } from 'react-router-dom';
import PageNotFoundImage from 'assets/images/PageNotFound.png';

import './styles.scss';

class NotFound extends React.Component {
  render() {
    return (
      <div className="not-found-container">
        <div className="img-wrapper">
          <img src={PageNotFoundImage} alt="" />
        </div>
        <p className="text-center">
          <Link to="/"> Повернутись на головну</Link>
        </p>
      </div>
    );
  }
}
export default NotFound;
