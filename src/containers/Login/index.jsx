import React, { useState } from "react";
import { Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import { toast } from "react-toastify";
// import { defaultPages } from "utils/data";
import BeatLoader from "react-spinners/BeatLoader";

import * as userActions from "actions/user";

import "./styles.scss";

function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);

  function validateForm() {
    return email.length > 2 && password.length > 2;
  }

  async function handleSubmit(ev) {
    ev.preventDefault();
    setLoading(true);
    try {
        await userActions.loginUser(email, password);
        props.history.push('/');
    } catch (error) {
      console.error(error);
      setLoading(false);
    } finally {
      // setLoading(false);
    }
  }

  const logged = localStorage.getItem('user');

  return logged && logged['id'] ? (
    <Redirect to='/' />
  ) : (
    <div className="login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email">
          <FormLabel>Email</FormLabel>
          <FormControl
            autoFocus
            type="text"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password">
          <FormLabel>Password</FormLabel>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button
          variant="secondary"
          block
          disabled={!validateForm() || loading}
          type="submit"
        >
          {loading ? <BeatLoader /> : "Login"}
        </Button>
      </form>
    </div>
  );
}

export default connect(state => ({
  user: state.user
}))(Login);
