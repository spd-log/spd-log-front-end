import { applyMiddleware, createStore } from 'redux'
import { createLogger } from 'redux-logger'
import ReduxThunk from 'redux-thunk'
import { rootReducer, initialState } from './reducers'

const logger = createLogger({
  collapsed: true,
})

const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(ReduxThunk, logger)
)

export default store
