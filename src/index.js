import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import App from './App';
import * as serviceWorker from './serviceWorker';

//normalize and some basic styles

import 'bootstrap/dist/css/bootstrap.min.css'
import 'assets/styles/index.scss'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
