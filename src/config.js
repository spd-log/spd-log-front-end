import axios from 'axios'

export const api = axios.create({
  baseURL: '/api'
})

export const getUser = () => {
  const user = JSON.parse(localStorage.getItem('user'));
  return user;
}

// export const setAuthHeaders = token => {
//   api.defaults.headers.common['Authorization'] = `Bearer ${token}`
// }

// const token = getToken()
// if (token) {
//   setAuthHeaders(token)
// }
