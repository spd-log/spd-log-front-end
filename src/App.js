import React, { useEffect, useState } from 'react'
import history from 'utils/history'
import { Router, Switch, Route } from 'react-router-dom'

import Login from 'containers/Login'
import PrivateRoute from 'components/PrivateRoute'
import Dashboard from 'containers/Dashboard'
import NotFound from 'containers/NotFound'
import Lectures from 'containers/Lectures'
import Calendar from 'containers/Calendar'
import SingleLecture from 'containers/Lectures/SingleLecture'
import Sidebar from 'components/Sidebar'
import Journal from 'containers/Journal'

import * as userActions from 'actions/user'


function App(props) {
  const [user, setUser] = useState(null);

  useEffect(() => {
    loadUser()
  }, [])

  const loadUser = async () => {
    try {
      const user = await userActions.authUser();
      setUser(user);
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <>
      <Router history={history}>
        <div className="app-container">
          <Sidebar />
          <div className="page-content">
            <Switch>
              <Route path="/login" exact component={Login} />
              <Route
                exact
                path="/"
                component={Dashboard}
              />
              <Route
                path="/lectures/:id"
                component={SingleLecture}
              />
              <Route
                exact
                path="/lectures"
                component={Lectures}
              />
              <Route
                exact
                path="/calendar"
                component={Calendar}
              />
              <Route
                exact
                path="/journal"
                component={Journal}
              />
              <Route path="*" component={NotFound} />
            </Switch>
          </div>
        </div>
      </Router>
    </>
  )
}


export default App
